#ifndef _Model_H_
#define _Model_H_

#include "ray.h"
#include <QObject>
#include <QList>
#include <QVector>
#include <QVector3D>
#include <QString>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QMatrix4x4>

/*******************************************************************************
  Model
  ******************************************************************************/

class Model : public QObject, protected QOpenGLFunctions
{
Q_OBJECT

public:
    // ctor
    Model();
    Model(QString& objFilename, QOpenGLShaderProgram* program);
    ~Model();

    // Methods
    void loadOBJ();
    void loadOBJ(const QString& objFilename);
    void saveOBJ(const QString& objFilename);
    void draw(QOpenGLShaderProgram* program, int wireframe=0);
    void populateColorsPerVertex(int targetPointID);
    void populateColorsWithBar();
    const QVector3D* constColorData() const;
    const GLushort* constIndexData() const;

    QMatrix4x4& getWorldMatrix() {return m_world;}
    QList<int> intersectWithRay(Ray& r, int& closest);

    //void setRandomCorrelations();
    void setWireframe(int isWireframe) { m_wireframe = isWireframe;}
    int countVertices() const;
    int countNormals() const;
    //int vertexCount() const;

public:
    // Data members
    //QList< QVector<float> > m_correlations;
    QVector<GLushort> m_indices;
    int m_offset;

private:
    // Methods
    bool intersectTriWithRay(Ray& r, int triNum, float& tau, int& closest);
    void setupVertexAttribs();
    void calculateNormals();
    void updateColors();

private:
    // Data members
    QString m_objFilename;
    QVector<QVector3D> m_vertices;
    QVector<QVector3D> m_normals;
    QVector<QVector3D> m_colors;
    QVector<GLuint> m_indices;

    // Transformation matrix that converts local coordinates to world coordinates
    QMatrix4x4 m_world;

    // OpenGL stuff
    QOpenGLVertexArrayObject m_vao;
    QOpenGLBuffer m_vertexBuf;
    QOpenGLBuffer m_normalBuf;
    QOpenGLBuffer m_colorBuf;
    QOpenGLBuffer m_indexBuf;

    //int m_selectedPoint;

public slots:
    void cleanup();

signals:
    void colorsChanged();
};

#endif
