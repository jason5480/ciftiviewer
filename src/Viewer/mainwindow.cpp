#include "mainwindow.h"
#include "glwidget.h"

#include <QMenuBar>
#include <QMenu>
#include <QMessageBox>
#include <QFileDialog>
#include <QDialog>
#include <QTimer>
#include <QDebug>

MainWindow::MainWindow() : m_timeWindowframe(0),
                           m_lastFrame(1000)
{
    setupUi(this);

    //m_timer.setInterval(100);
    //connect(m_pushButtonPlayPause, SIGNAL(pressed()), openGLWidget->m_timer, SLOT(start()));
    //connect(m_pushButtonPlayPause, SIGNAL(QPushButton::released, openGLWidget->m_timer, &QTimer::stop);
    //connect(openGLWidget->m_timer, &QBasicTimer::timeout, this, &MainWindow::animate);
}

MainWindow::~MainWindow()
{}

void MainWindow::on_actionCifti_triggered()
{
    QString dir = QString("D:/MRI/HCP/HCP_WB_Tutorial_1.0/");
    QString ciftiFilename = QFileDialog::getOpenFileName(this, tr("Select surface file"), dir, tr("Cifti Files (*.nii)"));

    if (!ciftiFilename.isEmpty())
    {
        qDebug() << "Cifti Here";
    }
}

void MainWindow::on_actionGifti_triggered()
{
    QString dir = QString("D:/MRI/HCP/HCP_WB_Tutorial_1.0/");
    QString giftiFilename = QFileDialog::getOpenFileName(this, tr("Select surface file"), dir, tr("Gifti Files (*.surf.gii)"));
    if (!giftiFilename.isEmpty())
    {
        qDebug() << "Gifti Here" << giftiFilename;
    }
}

void MainWindow::on_actionSaveAs_triggered()
{
    qDebug() << "SaveAs Here";
}

void MainWindow::on_m_checkBoxLock_toggled(bool state)
{
    openGLWidget->m_lock = state;
}

void MainWindow::on_m_sliderTimebar_valueChanged(int val)
{
    openGLWidget->getModel()->m_offset = val;
    m_timeWindowframe = val;
    openGLWidget->getModel()->populateColorsWithBar();
    openGLWidget->update();
}

void MainWindow::animate()
{
    qDebug() << "Animate" << endl;
    // Stop animation or repeat depending on checkbox value
    if (m_timeWindowframe == m_lastFrame)
    {
        m_timeWindowframe = 0;
        // Stop
        //if (!m_checkboxRepeat->isChecked())
        //{
            m_animOn = false;
            m_sliderTimebar->setValue(m_sliderTimebar->maximum());
            m_pushButtonPlayPause->setText("Play");
            m_pushButtonPlayPause->setIcon(QIcon(":/Icons/play.png"));
            m_sliderTimebar->setValue(m_timeWindowframe);

            // Call update so that changes will be rendered
            openGLWidget->update();
            return;
        //}
    }

    // Update edges/particles
    //emit newTimeWindowFrame(m_timeWindowframe);

    // Update timebar
    m_sliderTimebar->setValue(m_timeWindowframe);

    m_timeWindowframe++;

}