#ifndef _GLWIDGET_H_
#define _GLWIDGET_H_

#include "model.h"
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QWheelEvent>
#include <QKeyEvent>
#include <QInputEvent>
#include <QPoint>
#include <QVector2D>
#include <QVector3D>
#include <QQuaternion>
#include <QMatrix4x4>
#include <QString>
#include <QBasicTimer>

/*******************************************************************************
  GLWidget
  ******************************************************************************/

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
Q_OBJECT

public:
    // ctor
    GLWidget(QWidget* parent = 0);
    ~GLWidget();
    
    // Methods
    QSize minimumSizeHint() const Q_DECL_OVERRIDE;
    QSize sizeHint() const Q_DECL_OVERRIDE;
    Model* getModel(){ return m_model; }
    void castRay(const QPoint& pos, Ray& r);

public:
    int m_wireframe;
    bool m_lock;
    // The Timer
    QBasicTimer m_timer;

protected:
    // OpenGL framework Methods 
    void initializeGL() Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;
    void resizeGL(int width, int height) Q_DECL_OVERRIDE;
    // IO handle Methods
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    //void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void keyReleaseEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    int  mkModif(QInputEvent *event);
    // Timer Event handle
    //void timerEvent(QTimerEvent *e) Q_DECL_OVERRIDE;

private:
    // Methods
    void initShaders();
    void initModel();

private:
    // Shader program
    QOpenGLShaderProgram m_program;
    // Model
    Model* m_model;

    // Camera properties
    QVector3D m_eye;
    QVector3D m_focal;
    QVector3D m_upvec;
    QVector3D m_translation;
    float m_cDist;
    float m_zoomStep;

    QVector3D m_rotationAxis;
    qreal m_angle;
    QQuaternion m_rotation;

    // Transformation Matrices
    QMatrix4x4 m_projectionMatrix;
    QMatrix4x4 m_mvMatrix;
    QMatrix4x4 m_cameraMatrix;
    
    // Window Properties
    QVector2D m_lastPos;
    int m_lastClosestPointID;
    bool m_transparent;
    
public slots:
    void cleanup();

signals:
    void wireframeChanged(Qt::CheckState state);
};

#endif
