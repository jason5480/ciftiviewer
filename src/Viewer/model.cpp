#include "model.h"
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QFile>
#include <QTextStream>
#include <QDebug>

/*******************************************************************************
  Implementation of Model methods
  ******************************************************************************/

Model::Model()
    : m_vertexBuf(QOpenGLBuffer::VertexBuffer),
      m_normalBuf(QOpenGLBuffer::VertexBuffer),
      m_colorBuf(QOpenGLBuffer::VertexBuffer),
      m_indexBuf(QOpenGLBuffer::IndexBuffer),
      m_offset(0)
{
    initializeOpenGLFunctions();

    // Create a vertex array object. In OpenGL ES 2.0 and OpenGL 2.x
    // implementations this is optional and support may not be present
    // at all. Nonetheless the below code works in all cases and makes
    // sure there is a VAO when one is needed.
    m_vao.create();
    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);

    // Generate 4 VBOs
    m_vertexBuf.create();
    m_normalBuf.create();
    m_colorBuf.create();
    m_indexBuf.create();
}

Model::Model(QString& objFilename, QOpenGLShaderProgram* program)
    : m_vertexBuf(QOpenGLBuffer::VertexBuffer),
      m_normalBuf(QOpenGLBuffer::VertexBuffer),
      m_colorBuf(QOpenGLBuffer::VertexBuffer),
      m_indexBuf(QOpenGLBuffer::IndexBuffer),
      m_offset(0)
{
    
    initializeOpenGLFunctions();
    
    // Create a vertex array object. In OpenGL ES 2.0 and OpenGL 2.x
    // implementations this is optional and support may not be present
    // at all. Nonetheless the below code works in all cases and makes
    // sure there is a VAO when one is needed.
    m_vao.create();
    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);

    // Generate 4 VBOs
    m_vertexBuf.create();
    m_normalBuf.create();
    m_colorBuf.create();
    m_indexBuf.create();

    loadOBJ(objFilename);

    setupVertexAttribs();

    // Tell OpenGL which VBOs to use
    m_vertexBuf.bind();

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int vertexLocation = program->attributeLocation("vertexPosition_modelspace");
    program->setAttributeBuffer(vertexLocation, GL_FLOAT, 0, 3, sizeof(QVector3D));
    program->enableAttributeArray(vertexLocation);

    // Tell OpenGL which VBOs to use
    m_normalBuf.bind();

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int normalLocation = program->attributeLocation("vertexNormal_modelspace");
    program->setAttributeBuffer(normalLocation, GL_FLOAT, 0, 3, sizeof(QVector3D));
    program->enableAttributeArray(normalLocation);

    // Tell OpenGL which VBOs to use
    m_colorBuf.bind();

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int colorLocation = program->attributeLocation("vertexColor");
    program->setAttributeBuffer(colorLocation, GL_FLOAT, 0, 3, sizeof(QVector3D));
    program->enableAttributeArray(colorLocation);

    // Set the world transformation matrix
    m_world.setToIdentity();
    m_world.rotate(-90, 0, 0, 1);
    m_world.rotate(-90, 0, 1, 0);
}

Model::~Model()
{
    cleanup();
}

void Model::cleanup()
{
    // Destroy 4 VBOs
    m_vertexBuf.destroy();
    m_normalBuf.destroy();
    m_colorBuf.destroy();
    m_indexBuf.destroy();
}

void Model::loadOBJ()
{
    qDebug() << "Model " << m_objFilename << " is Loading...";
    QFile file(m_objFilename);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);
        in.setRealNumberPrecision(6);
        QVector<QVector3D> tempNormals;
        qDebug() << __LINE__;
        while (!in.atEnd())
        {
            QString line = in.readLine();
            // If line is empty go to the next one
            if (line.isEmpty()) continue;
            // Split line in tokens
            QStringList tokens = line.split(" ", QString::SkipEmptyParts);
            //*
            if (tokens[0] == "v")
            {
                m_vertices.push_back(QVector3D(tokens[1].toFloat(),
                                               tokens[2].toFloat(),
                                               tokens[3].toFloat()));

                m_colors.push_back(QVector3D(0.8f, 0.8f, 0.8f));
            }
            else if (tokens[0] == "vn")
            {
                tempNormals.push_back(QVector3D(tokens[1].toFloat(), tokens[2].toFloat(), tokens[3].toFloat()));
            }
            else if (tokens[0] == "f")
            {
                QStringList tok1, tok2, tok3;
                tok1 = tokens[1].split("/", QString::KeepEmptyParts);
                tok2 = tokens[2].split("/", QString::KeepEmptyParts);
                tok3 = tokens[3].split("/", QString::KeepEmptyParts);

                int v1 = tok1[0].toInt() - 1;
                int v2 = tok2[0].toInt() - 1;
                int v3 = tok3[0].toInt() - 1;

                m_indices.push_back(v1);
                m_indices.push_back(v2);
                m_indices.push_back(v3);

                m_indices.push_back(v1);
                m_indices.push_back(v2);
                m_indices.push_back(v3);

                m_indices.push_back(n1);
                m_indices.push_back(n2);
                m_indices.push_back(n3);

            }
        }
        file.close();
        qDebug() << "Model " << m_vertices.size() << tempNormals.size();
        if (m_vertices.size() == tempNormals.size()) m_normals = tempNormals;
        else
            calculateNormals();

        qDebug() << "Model " << m_objFilename << " Loaded successfully!!" << m_vertices.count() << m_normals.count() << m_colors.count() << m_indices.count()/3;
    }
    else
    {
        qDebug() << "Unable to open file: " << m_objFilename;
        file.close();
    }
}

void Model::loadOBJ(const QString& objFilename)
{
    this->m_objFilename = objFilename;
    this->loadOBJ();
}

void Model::saveOBJ(const QString& objFilename)
{
    qDebug() << "Exporting OBJ...";

    QFile file(objFilename + QString("Copy.obj"));
    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream out(&file);
        out << (objFilename + QString("Copy.obj")) << endl;
        out.setRealNumberNotation(QTextStream::FixedNotation);
        out.setRealNumberPrecision(6);
        for (int i = 0; i < m_vertices.size(); i++)
        {
            out << "v" << " " << m_vertices[i].x()
                << " " << m_vertices[i].y()
                << " " << m_vertices[i].z() << endl;
        }
        for (int i = 0; i < (int)m_normals.size(); i++)
        {
            out << "vn" << " " << m_normals[i].x()
                << " " << m_normals[i].y()
                << " " << m_normals[i].z() << endl;
        }
        out << "s 1" << endl;
        for (int i = 0; i < (int)m_indices.size(); i++)
        {
            out << "f" << " " << m_indices[i] + 1 << "//" << m_indices[i] + 1
                << " " << m_indices[i + 1] + 1 << "//" << m_indices[i+1] + 1
                << " " << m_indices[i + 2] + 1 << "//" << m_indices[i+2] + 1 << endl;
        }
        file.close();
    }

    qDebug() << "OBJ Export Complete";
}

void Model::draw(QOpenGLShaderProgram* program, int wireframe)
    /*
{
    // Bind the program
    program->bind();
    //*/

    // Bind the VAO
    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);
    
    // Tell OpenGL which VBOs to use
    m_vertexBuf.bind();
    m_normalBuf.bind();
    m_colorBuf.bind();
    m_indexBuf.bind();

    if (wireframe == Qt::Unchecked)
    {
        program->setUniformValue("isWireframe", (GLfloat)0.0);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        // Draw cube geometry using indices from VBO 3
        glDrawElements(GL_TRIANGLES, m_indices.count(), GL_UNSIGNED_INT, 0);
    }
    else if (wireframe == Qt::PartiallyChecked)
    {
        program->setUniformValue("isWireframe", (GLfloat)0.0);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        // Draw cube geometry using indices from VBO 3
        glDrawElements(GL_TRIANGLES, m_indices.count(), GL_UNSIGNED_INT, 0);
        program->setUniformValue("isWireframe", (GLfloat)1.0);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        // Draw cube geometry using indices from VBO 3
        glDrawElements(GL_TRIANGLES, m_indices.count(), GL_UNSIGNED_INT, 0);
    }
    else if (wireframe == Qt::Checked)
    {
        program->setUniformValue("isWireframe", (GLfloat)0.0);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        // Draw cube geometry using indices from VBO 3
        glDrawElements(GL_TRIANGLES, m_indices.count(), GL_UNSIGNED_INT, 0);
    }
    // Release the buffers
    m_vertexBuf.release();
    m_normalBuf.release();
    m_colorBuf.release();
    m_indexBuf.release();
    // Release the program
    program->release();
}

void Model::populateColorsPerVertex(int targetPointID)
{
    if (!m_colors.empty()) m_colors.clear();

    qDebug() << "Colors per vertex populating..";

    // Give color based on y value in modelspace
    for (int i = 0; i < m_vertices.size(); i++)
    {
        m_colors.push_back(QVector3D(m_vertices[targetPointID].distanceToPoint(m_vertices[i])/100., 1, 0.5));
    }
    qDebug() << "Colors populated sucessfully" << m_colors.count();
    updateColors();
}

void Model::populateColorsWithBar()
{
    if (!m_colors.empty()) m_colors.clear();

    qDebug() << "Colors with Bar populating..";

    // Give color based on the y coordinate in modelspace
    for (int i = 0; i < m_vertices.size(); i++)
    {
        m_colors.push_back(QVector3D((m_vertices[i].y() + m_offset + 100.)/200., 1, 0.5));
    }
    qDebug() << "Colors populated sucessfully" << m_colors.count();
    updateColors();
}

QList<int> Model::intersectWithRay(Ray& r, int& closest)
{
    QList<int> triList;
    QList<float> tauList;
    QList<int> closestList;

    float tau;
    int closestPoinID;
    for (int i = 0; i < m_indices.size()/3; i++)
{
    //return m_data.constData();
    return m_normals.constData();
}

//------------------------------------------------------------------------------

const QVector3D* Model::constColorData() const
    {
        if (intersectTriWithRay(r, i, tau, closestPoinID))
        {
            triList.push_back(i);
            tauList.push_back(tau);
            closestList.push_back(closestPoinID);
        }
//------------------------------------------------------------------------------

void Model::draw() const
{
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    qDebug() << __LINE__ << "Before Draw";
    glDrawElements(GL_TRIANGLES, m_indices.count(), GL_UNSIGNED_SHORT, m_indices.constData());
    //glDrawArrays(GL_TRIANGLES, 0, vertexCount());
    qDebug() << __LINE__ << "After Draw";
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
}

//------------------------------------------------------------------------------

int Model::countVertices() const
{
    return m_vertices.count();
}

//------------------------------------------------------------------------------

int Model::countNormals() const
{
    return m_normals.count();
    }

    // Sort the List using quick bubblesort
    int n = triList.size();
    do
    {
        int newn = 0;
        for (int i = 1; i < n; i++)
        {
            if (tauList[i - 1] > tauList[i])
            {
                tauList.swap(i - 1, i);
                triList.swap(i - 1, i);
                closestList.swap(i - 1, i);
                newn = i;
            }
        }
        n = newn;
    } while (n != 0);

    closest = closestList[0];

    return triList;
}

//int Model::vertexCount() const
//{
//    //return m_count / 6;
//    return m_vertices.count();
//}

bool Model::intersectTriWithRay(Ray& r, int triNum, float& tau, int& closest)
{
    QVector3D& e = r.start();
    QVector3D& d = r.dir();

    QVector3D &a = m_vertices[m_indices[3 * triNum]];
    QVector3D &b = m_vertices[m_indices[3 * triNum + 1]];
    QVector3D &c = m_vertices[m_indices[3 * triNum + 2]];

    // Solve the 3x3 equation e + t*d = a + gi*(b - a) + vi*(c - a)
    float xaxb = a.x() - b.x();
    float yayb = a.y() - b.y();
    float zazb = a.z() - b.z();
    float xaxc = a.x() - c.x();
    float yayc = a.y() - c.y();
    float zazc = a.z() - c.z();
    float xd = d.x();
    float yd = d.y();
    float zd = d.z();
    float xaxe = a.x() - e.x();
    float yaye = a.y() - e.y();
    float zaze = a.z() - e.z();

    float D = xaxb * (yayc * zd - yd   * zazc)
        - xaxc * (yayb * zd - yd   * zazb)
        + xd * (yayb * zazc - yayc * zazb);

    float Dg = xaxb * (yaye * zd - yd   * zaze)
        - xaxe * (yayb * zd - yd   * zazb)
        + xd   * (yayb * zaze - yaye * zazb);
    float vi = Dg / D;
    if (vi < 0 || vi > 1) return false;

    float Db = xaxe * (yayc * zd - yd   * zazc)
        - xaxc * (yaye * zd - yd   * zaze)
        + xd   * (yaye * zazc - yayc * zaze);
    float gi = Db / D;
    if (gi < 0 || gi > 1 - vi) return false;

    // There are three medians described in barycentric coordinates
    // gi = vi, gi = -2 * vi + 1, gi = -0.5 * vi + 0.5
    if (gi < vi)
    {
        if (gi < -2 * vi + 1) closest = m_indices[3 * triNum];
        else closest = m_indices[3 * triNum + 2];
    }
    else
    {
        if (gi < -0.5 * vi + 0.5) closest = m_indices[3 * triNum];
        else closest = m_indices[3 * triNum + 1];
    }

    float Dt = xaxb * (yayc * zaze - yaye * zazc)
        - xaxc * (yayb * zaze - yaye * zazb)
        + xaxe * (yayb * zazc - yayc * zazb);

    tau = Dt / D;


    return true;
}

void Model::setupVertexAttribs()
{
    // Transfer vertex data to VBO 0
    m_vertexBuf.bind();
    m_vertexBuf.allocate(m_vertices.constData(), m_vertices.count() * sizeof(QVector3D));
    m_vertexBuf.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_vertexBuf.release();

    // Transfer vertex data to VBO 1
    m_normalBuf.bind();
    m_normalBuf.allocate(m_normals.constData(), m_normals.count() * sizeof(QVector3D));
    m_normalBuf.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_normalBuf.release();

    // Transfer vertex data to VBO 2
    m_colorBuf.bind();
    m_colorBuf.allocate(m_colors.constData(), m_colors.count() * sizeof(QVector3D));
    m_colorBuf.release();

    // Transfer index data to VBO 3
    m_indexBuf.bind();
    m_indexBuf.allocate(m_indices.constData(), m_indices.count()* sizeof(GLuint));
    m_indexBuf.release();
}

void Model::calculateNormals()
{   m_normals.fill(QVector3D(), m_vertices.size());
    qDebug() << "Calculating Normals..." << m_normals.size();
    for (int i = 0; i < m_indices.size()/3; i+=3)
    {
        QVector3D norm = QVector3D::crossProduct((m_vertices[m_indices[i+1]] - m_vertices[m_indices[i]]),
                                                 (m_vertices[m_indices[i+2]] - m_vertices[m_indices[i]]));
        m_normals[m_indices[i]]   += norm;
        m_normals[m_indices[i+1]] += norm;
        m_normals[m_indices[i+2]] += norm;
    }
    for (int i = 0; i < m_normals.size(); i++)
    {
        m_normals[i].normalize();
    }
    qDebug() << "Normals Calculated sucessfully" << m_normals.size();
}

void Model::updateColors()
{
    m_colorBuf.bind();
    m_colorBuf.write(0, m_colors.constData(), m_colors.count() * sizeof(QVector3D));
    m_colorBuf.release();
}

//void Model::setRandomCorrelations()
//{
//    qDebug() << "Creating correlations:" << m_correlations.size() << m_vertices.size();
//    for (int i = 0; i < m_vertices.size(); i++)
//    {
//        QVector<float> tempi;
//        for (int j = 1; j < m_vertices.size()-i; j++)
//        {
//            tempi.push_back(m_vertices[i].distanceToPoint(m_vertices[j]));
//        }
//        m_correlations.push_back(tempi);
//    }
//    qDebug() << "Correlations created" << m_correlations.size();
//}