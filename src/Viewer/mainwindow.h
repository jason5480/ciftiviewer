#ifndef _MAINWINDOW_H_
#define _MAINWINDOW_H_

#include "ui_mainwindow.h"

#include <QMainWindow>
#include <QBasicTimer>

/*******************************************************************************
  MainWindow
  ******************************************************************************/

class MainWindow : public QMainWindow, private Ui::MainWindow
{
Q_OBJECT

public:
    // ctor
    MainWindow();
    ~MainWindow();

public:
    //QTimer m_timer;

    bool m_animOn;
    int m_timeWindowframe;
    int m_lastFrame;

private slots:
    void on_actionCifti_triggered();
    void on_actionGifti_triggered();
    void on_actionSaveAs_triggered();
    void on_m_checkBoxLock_toggled(bool state);
    //void onPlayPauseClicked();
    void on_m_sliderTimebar_valueChanged(int val);
    //void onValueChanged(double val);
    void animate();

signals:
};

#endif
