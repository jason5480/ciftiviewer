#include "glwidget.h"
#include "ray.h"

#include <QMouseEvent>
#include <QOpenGLShaderProgram>
#include <QCoreApplication>
#include <math.h>
#define PROGRAM_VERTEXINDEX_ATTRIBUTE 3
#define PROGRAM_NORMALINDEX_ATTRIBUTE 4

GLWidget::GLWidget(QWidget *parent)
    : QOpenGLWidget(parent),
      m_rotation(),
      m_translation(),
      m_angle(0),
      m_model(0),
                                      m_modelVertexVbo(QOpenGLBuffer::VertexBuffer),
                                      m_modelNormalVbo(QOpenGLBuffer::VertexBuffer),
                                      m_modelColorVbo(QOpenGLBuffer::VertexBuffer),
                                      m_modelIndicesVbo(QOpenGLBuffer::IndexBuffer),
      m_cDist(300),
      m_zoomStep(5),
      m_wireframe(0),
      m_lock(0),
      m_transparent(false)

{
    setFocusPolicy(Qt::WheelFocus);
                                                         m_modelVertexVbo(QOpenGLBuffer::VertexBuffer),
                                                         m_modelNormalVbo(QOpenGLBuffer::VertexBuffer),
                                                         m_modelColorVbo(QOpenGLBuffer::VertexBuffer),
                                                         m_modelIndicesVbo(QOpenGLBuffer::IndexBuffer),

    if (m_transparent) setAttribute(Qt::WA_TranslucentBackground);
}

GLWidget::~GLWidget()
{
    cleanup();
}

void GLWidget::cleanup()
{
    // Make sure the context is current when deleting the buffers.
    makeCurrent();
    delete m_model;
    m_model = NULL;
    doneCurrent();
}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize GLWidget::sizeHint() const
{
    return QSize(800, 600);
}

void GLWidget::castRay(const QPoint& pos, Ray& r)
{
    int w = width();
    int h = height();
    // Constuct ray in camera space

    // Convert selected pixel from screen space to normalized [-1, 1] cliping space
    QVector3D target_clipingspace((pos.x() - (w / 2) + 0.5) / (0.5 * w),
                                (-(pos.y() - (h / 2) + 0.5) / (0.5 * h)),
                                                                      0);
    // Convert target to camera space
    QVector3D target_cameraspace = m_projectionMatrix.inverted() * target_clipingspace;

    // Convert ray from camera to model space
    QVector3D eye_modelspace = m_mvMatrix.inverted() * QVector3D(0, 0, 0);
    QVector3D target_modelspace = m_mvMatrix.inverted() * target_cameraspace;

    // Get the ray in model space
    r.set(eye_modelspace, target_modelspace);
    m_modelColorVbo.destroy();
    m_modelIndicesVbo.destroy();
}

void GLWidget::initializeGL()
{
    // In this example the widget's corresponding top-level window can change
    // several times during the widget's lifetime. Whenever this happens, the
    // QOpenGLWidget's associated context is destroyed and a new one is created.
    // Therefore we have to be prepared to clean up the resources on the
    // aboutToBeDestroyed() signal, instead of the destructor. The emission of
    // the signal will be followed by an invocation of initializeGL() where we
    // can recreate all resources.
    connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &GLWidget::cleanup);

    initializeOpenGLFunctions();

    // White backround
    glClearColor(0, 0, 0, m_transparent ? 0 : 1);

    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    // Cull triangles which normal is not towards the camera
    glEnable(GL_CULL_FACE);

    glLineWidth(1);

    initShaders();

    initModel();
    m_program->bindAttributeLocation("vertexIndex",               PROGRAM_VERTEXINDEX_ATTRIBUTE);
    m_program->bindAttributeLocation("normalIndex",               PROGRAM_NORMALINDEX_ATTRIBUTE);

    // Our camera changes in this example.
    m_cameraMatrix.setToIdentity();
    m_eye = QVector3D(0, 0, m_cDist);
    m_focal = QVector3D(0, 0, 0);
    m_upvec = QVector3D(0, 1, 0);
    m_cameraMatrix.lookAt(m_eye, m_focal, m_upvec);

    // Bind the program
    m_program.bind();

    // Light position is fixed.
    m_program.setUniformValue("lightPosition_cameraspace", QVector3D(0, 0, 0));
    m_program.setUniformValue("lightColor", QVector3D(1, 1, 1));
    m_program.setUniformValue("lightPower", GLfloat(30000));
    m_program.setUniformValue("isBlack", 0);

    m_program.release();
    m_modelVertexVbo.release();

    // Normal data
    m_modelNormalVbo.bind();
    m_modelNormalVbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_modelNormalVbo.allocate(m_model.constNormalData(), m_model.countNormals() * sizeof(GLfloat) * 3);
    m_modelNormalVbo.release();

    // Color data
    m_modelColorVbo.bind();
    m_modelColorVbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_modelColorVbo.allocate(m_model.constColorData(), m_model.countColors() * sizeof(GLfloat) * 3);
    m_modelColorVbo.release();
    
    // Indices
    m_modelIndicesVbo.bind();
    m_modelIndicesVbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_modelIndicesVbo.allocate(m_model.constIndexData(), m_model.countIndices() * sizeof(GLushort));
    m_modelIndicesVbo.release();

    // Use QBasicTimer because its faster than QTimer
    m_timer.start(12, this);
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Get the current world Matrix transformation from model
    QMatrix4x4 worldMatrix = m_model->getWorldMatrix();

    // Set the camera
    m_cameraMatrix.setToIdentity();
    m_eye = QVector3D(0, 0, m_cDist);
    m_cameraMatrix.lookAt(m_eye + m_translation, m_focal + m_translation, m_upvec);
    //m_cameraMatrix.translate(m_translation);
    m_cameraMatrix.rotate(m_rotation);

    m_mvMatrix = m_cameraMatrix * worldMatrix;

    m_program.bind();
    m_program.setUniformValue("projMatrix", m_projectionMatrix);
    m_program.setUniformValue("mvMatrix", m_mvMatrix);
    m_program.setUniformValue("viewMatrix", m_cameraMatrix);
    m_program.setUniformValue("modelMatrix", worldMatrix);
    m_program.setUniformValue("normalMatrix", m_mvMatrix.normalMatrix());

    m_model->draw(&m_program, m_wireframe);

    m_program.release();
}

void GLWidget::resizeGL(int w, int h)
{
    // Calculate aspect ratio
    qreal aspect = qreal(w) / qreal(h ? h : 1);
    // Set near plane to 1.0, far plane to 500, field of view 45 degrees
    const qreal zNear = 1.0, zFar = 500.0, fov = 45.0;
    // Reset projection
    m_projectionMatrix.setToIdentity();
    // Set perspective projection
    m_projectionMatrix.perspective(fov, aspect, zNear, zFar);
    //glVertexAttribPointer(PROGRAM_VERTEX_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, sizeof(QVector3D), 0);
    glVertexPointer(3, GL_FLOAT, 0, 0);
    m_modelVertexVbo.release();
    
    m_modelNormalVbo.bind();
    //glVertexAttribPointer(PROGRAM_NORMAL_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, sizeof(QVector3D), 0);
    glNormalPointer(GL_FLOAT, 0, 0);
    m_modelNormalVbo.release();

    //m_modelIndicesVbo.bind();
    //glVertexAttribPointer(PROGRAM_VERTEXINDEX_ATTRIBUTE, 1, GL_INT, GL_FALSE, 3 * sizeof(int), 0);
    //glVertexAttribPointer(PROGRAM_NORMALINDEX_ATTRIBUTE, 1, GL_INT, GL_FALSE, 3 * sizeof(int), reinterpret_cast<void*>(3 * sizeof(int)));
    //m_modelIndicesVbo.release();

    m_modelColorVbo.bind();
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    if ((event->buttons() & Qt::RightButton) && !(event->modifiers() & Qt::ControlModifier))
    {
        Ray ray_modelspace;
        castRay(event->pos(), ray_modelspace);

        // Get a list with intersecting triangles
        int closestPointID;
        QList<int> triainter = m_model->intersectWithRay(ray_modelspace, closestPointID);

        if (triainter.size())
        {
            m_model->populateColorsPerVertex(closestPointID);
            m_lastClosestPointID = closestPointID;
            update();
        }
        else qDebug() << "No intersection";
    }

    m_lastPos = QVector2D(event->localPos());
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (!m_lock)
    {
        int modif = mkModif(event);

        // Mouse release position - mouse press position
        QVector2D diff = QVector2D(event->localPos()) - m_lastPos;

        if (modif)
        {
            QVector3D n = (m_focal - m_eye).normalized();
            QVector3D d(-diff.x(), diff.y(), 0);
            m_translation += d; //QVector3D::crossProduct(n, m_upvec)
            update();
        }
        else
        {
            if (event->buttons() & Qt::LeftButton)
            {
                // Rotation axis is perpendicular to the mouse position difference
                // vector
                m_rotationAxis = QVector3D(diff.y(), diff.x(), 0.0).normalized();

                // Accelerate angular speed relative to the length of the mouse sweep
                qreal m_angle = diff.length() / 10.0;

                // Update rotation
                m_rotation = QQuaternion::fromAxisAndAngle(m_rotationAxis, m_angle) * m_rotation;
                update();
            }
            if (event->buttons() & Qt::RightButton)
            {
                Ray ray_modelspace;
                castRay(event->pos(), ray_modelspace);

                // Get a list with intersecting triangles
                int closestPointID;
                QList<int> triainter = m_model->intersectWithRay(ray_modelspace, closestPointID);

                if (triainter.size())
                {
                    if (closestPointID != m_lastClosestPointID)
        m_modelIndicesVbo.bind();
        m_model.draw();
        m_modelIndicesVbo.release();
                    {
                        m_model->populateColorsPerVertex(closestPointID);
                        m_lastClosestPointID = closestPointID;
                        update();
                    }
                }
                else qDebug() << "No intersection";
        m_modelIndicesVbo.release();
            }
        }
    }
    m_lastPos = QVector2D(event->localPos());
        m_model.draw();
        m_modelIndicesVbo.release();
}

/*
void GLWidget::mouseReleaseEvent(QMouseEvent *event)
{
    // Mouse release position - mouse press position
    QVector2D diff = QVector2D(event->localPos()) - m_lastPos;

    // Rotation axis is perpendicular to the mouse position difference
    // vector
    QVector3D n = QVector3D(diff.y(), diff.x(), 0.0).normalized();

    // Accelerate angular speed relative to the length of the mouse sweep
    qreal acc = diff.length() / 100.0;

    // Calculate new rotation axis as weighted sum
    m_rotationAxis = (m_rotationAxis * m_angularSpeed + n * acc).normalized();

    // Increase angular speed
    m_angularSpeed += acc;
}
//*/

void GLWidget::wheelEvent(QWheelEvent *event)
{
    m_cDist -= m_zoomStep * (event->delta() / 120);
    if (m_cDist < 10) m_cDist = 10;
    if (m_cDist > 800) m_cDist = 800;

    update();
}

void GLWidget::keyPressEvent(QKeyEvent *event)
{
    int modif = mkModif(event);
    QString c = event->text();
    unsigned char key = c.toLatin1()[0];
    //*
    switch (isprint(key) ? tolower(key) : key)
    {
    case 'a':
        qDebug() << "a pressed";
        break;
    case 'w':
        m_wireframe = ++m_wireframe % 3;
        emit wireframeChanged((Qt::CheckState) m_wireframe);
        update();
        break;
    case 's':
        m_wireframe = --m_wireframe;
        if (m_wireframe < 0) m_wireframe = 2;
        emit wireframeChanged((Qt::CheckState) m_wireframe);
        update();
        break;
    case 'l':
        m_lock = !m_lock;
        break;
    case 'n':
        qDebug() << "n pressed";
        break;
    case 'b':
        qDebug() << "b pressed";
        break;
    }
    //if (event->key() == Qt::Key_Escape);
    //*/
}

void GLWidget::keyReleaseEvent(QKeyEvent *event)
{}

int GLWidget::mkModif(QInputEvent *event)
{
    int ctrl = event->modifiers() & Qt::ControlModifier ? 1 : 0;
    int shift = event->modifiers() & Qt::ShiftModifier ? 1 : 0;
    int alt = event->modifiers() & Qt::AltModifier ? 1 : 0;
    int modif = (ctrl << 0) | (shift << 1) | (alt << 2);
    return modif;
}

/*
void GLWidget::timerEvent(QTimerEvent *)
{
    // Decrease angular speed (friction)
    m_angularSpeed *= 0.99;

    // Stop rotation when speed goes below threshold
    if (m_angularSpeed < 0.01) {
        m_angularSpeed = 0.0;
    }
    else {
        // Update rotation
        m_rotation = QQuaternion::fromAxisAndAngle(m_rotationAxis, m_angle) * m_rotation;

        // Request an update
        update();
    }
}
//*/

void GLWidget::initShaders()
{
    if (!m_program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/vertShader.glsl"))
    {
        GLenum err = glGetError();
        qDebug() << "OpenGL ERROR No:" << err;
        close();
    }
    if (!m_program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/fragShader.glsl"))
    {
        GLenum err = glGetError();
        qDebug() << "OpenGL ERROR No:" << err;
        close();
    }
    if (!m_program.link())
    {
        GLenum err = glGetError();
        qDebug() << "OpenGL ERROR No:" << err;
        close();
    }
    if (!m_program.bind())
    {
        GLenum err = glGetError();
        qDebug() << "OpenGL ERROR No:" << err;
        close();
    }
    else m_program.release();
}

void GLWidget::initModel()
{
    if (m_model) delete m_model;
    // Load the model
    m_model = new Model(QString("E:/Workspace_VC/CiftiViewer/src/Viewer/Models/brain_average.obj"), &m_program);
}